# Wichtiges

- Bilder machen lassen
- Formatierung auf Kontaktformularseite
	- Bullet Points für Zoom usw.


# Weitere Ideen

- Onboarding
	- was wird genau gebraucht?
	- ungefähres Budget (ungefähr)

- anwählbarer Kalender inklusive Kundendatenbank usw
- nicht ami-server für Form

- SEO
	- ghostwriting
	- R consulting
	- R Hilfe
	- rstats 

# Kommentare Spatz


- Texte:
	- Dienstleistungen
		- R-Hilfe
			wie bereits vorhanden. Beispiel: LINK (Html-Output)

- DONE
	- Dienstleistungen
		- Datenvisualisierungen
			- Ich unterstütze dich, damit die gewonnenen Informationen aus deinen Daten auch visuell passend kommunizieren. 
		- Datenprojekte können mühsam sein. Damit du dein Projekt erfolgreich umsetzen kannst, begleite ich dich bei deiner Herausforderung - egal ob Seminar-, Bachelor- oder Masterarbeit.
		- Datenaufbereitung
			wie bereits vorhanden
		- Modellinterpretationen
			- Bei Bedarf unterstütze ich dich auch bei der Auswahl der passenden Methodologie (Spezialkondition: ab 160.-/h).
	- Grösse R-Logo 
	- Start
		- Gemeinsam meistern wir dein Datenprojekt.
		- ((Button)) Projektanfrage starten
	- Preise
	- Standard-Tarif für Studierende: CHF 40.-
	- Standard-Tarif für Firmen: CHF 80.-
	- Modellinterpretationen ab CHF 160.- pro Stunde
	- Express-Aufschlag (gewünschter Liefertermin <1 Woche): CHF 100.- pauschal
	- "About"-Section
	- "Egal wie komplex dein Projekt ist, ich nehme mir gerne Zeit. Ich gern via E-Mail, Telefon und Skype/Zoom/Teamviewer für dich da."
	- See Details auf Deutsch
	- "Heure mich an" - ""
	- "About" im Text löschen
	- nicht "jetzt R-Hilfe erhalten" sondern kongruent überall 
	- Kontaktformular:
	- Unternehmenskunde löschen
	- 1 optionales Feld "Firmenname (optional)"
	- Expressanfrage und "Wobei benötigst du Unterstützung"
		1. Auswahl "bitte auswhählen", im Hintergrund leer
	- zwei Icons im Footer löschen
	- E-Mail Adresse in Footer nicht schwarz
	- CTA auf Kontaktseite entfernen (oben und unten)
	- Arbeitsbeispiele in Menü integrieren
	- Cards
		- eine Zeile
	- Kontaktseite
	1. Card: Kontaktmöglichkeiten
		- Telefon
		- E-Mail
		- Skype, Zoom, Facetime 
		- Treffen im Raum Zürich
	2. Card: Verfügbarkeit
	3. Card: Sprache
		- Deutsch
		- Schweizerdeutsch
		- Englisch
	- Dienstleistungen so ordnen, nach Gewichtung (was du anbietest) überprüfen
		- Bachelorarbeiten, etc.

- balthasar.help auf Gitlab mappen


# Nächste Iteration
- Reprex
- Vlado fragen wenn Cross-Origin-Fehler angezeigt werden
- Progressbar in Preise anpassen
- SEO Optimierungen
- Future: übER MICH mit z.B. Angaben Studium
- Privat- und Firmenkunden trennen?

- neues Layout für Kontaktseite definieren, da Call to Action auch auf der Kontaktseite dargestellt wird
- nicetohave für Buchungen: https://calendly.com/de