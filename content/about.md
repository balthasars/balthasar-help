---
title: 'Über Balthasar'
---

Warum solltest Du mir vertrauen? 


## Erfahrung aus Studium und Praxis

Ich habe sowohl im Masterstudium, wie auch im Berufsleben (ich habe ein Jahr bei [cynkra GmbH](https://www.cynkra.com) gearbeitet) einiges an Erfahrung mit der Benutzung mit `R` gesammelt. 

In dieser Zeit habe ich die Best Practices bezüglich Workflow und verschiedenste Geschmacksrichtungen von R Code kennen gelernt.

## Geduld und angenehm lesbarer Code auf Deine Sprache

Ich meine, Dir Code verständlich erklären zu können und helfe gerne.
Bei mir gibt es keine wüsten For-Loops und  
 Ich bin geduldig und kenne alle Frustrationen, die `R` mit sich bringen können.

## Du weisst, was Du bezahlst

Wir machen von Anfang an einen Preis und den Umfang meiner Beratung ab. Der Basisstundenpreis beträgt 37.-, 

Wenn danach etwas nicht klappt, das dabei war, keine Sorge: Ich kümmere mich darum.