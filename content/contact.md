---
title: 'Projektanfrage starten'
date: 2018-02-22T17:01:34+07:00
layout: contact
---

Kontaktiere mich per Mail auf balthasar@balthasar.help mit einem Terminvorschlag und einem kurzen Problemumriss — ich antworte so schnell wie möglich. 

Für persönliche Beratungen bin ich am *Montag*, *Donnerstag*, *Freitag*, und unter Umständen auch am Wochenende verfügbar.